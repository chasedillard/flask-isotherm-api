import unittest
import os
import datetime
from sqlite3 import Connection
from templogging import LogComponent


def disable_test(func):
    """Decorator to disable tests (if needed). TODO: move to separate location."""

    def wrapper():
        func.__test__ = False
        return func

    return wrapper


def reset(db):
    """Undoes the mutations previous tests may have caused to the database."""
    db.open('test')
    db.delete_month_log()


class DatabaseTest(unittest.TestCase):
    def test_01_database_class(self):
        """Ensure that the class works properly. This test will fail if pipenv is not used properly."""
        db = LogComponent()
        self.assertTrue(isinstance(db, LogComponent))

    def test_02_datetime(self):
        """Ensure that datetime works properly in the context of the logging."""
        db = LogComponent()
        test_date = f'_{datetime.datetime.now().year}_{datetime.datetime.now().month}'
        self.assertEqual(test_date, db._log_suffix)

    def test_03_open_and_path(self):
        """Check that the log is saved in the correct location and is properly named."""
        db = LogComponent()
        db.open('test')

        # Ensure that suffix is properly created.
        suffix = f'_{datetime.datetime.now().year}_{datetime.datetime.now().month}'
        self.assertEqual(suffix, db._log_suffix)

        # Ensure that the path is properly calculated.
        unit_test_path = os.path.dirname(os.path.realpath(__file__))
        root_path = os.path.join(unit_test_path, os.pardir)
        log_path = os.path.join(root_path, 'logs')
        test_path = os.path.realpath(os.path.join(log_path, f'test{suffix}.db'))
        self.assertEqual(db.db_path, test_path)

        # Ensure that the database is properly instantiated.
        self.assertTrue(isinstance(db.get_db(), Connection))

    def test_04_delete(self):
        """Ensures that the database gets properly deleted."""
        db = LogComponent()
        db.open('test')
        db.delete_month_log()
        self.assertTrue(not os.path.exists(db.db_path))

    def test_05_connection(self):
        """Checks that the database connection is correctly made and does not throw exceptions."""
        db = LogComponent()
        db.open('test')
        self.assertTrue(db.get_db() is not None)

    def test_06_query(self):
        db = LogComponent()
        reset(db)
        db.open('test')
        query = "SELECT name FROM sqlite_master"

        # Ensure that there are no tables within the new database.
        dump = db.query(query)
        self.assertEqual([], dump)

    def test_07_initialization(self):
        """Test that the table is initialized properly."""
        query = "SELECT name FROM sqlite_master where type='table'"
        db = LogComponent()
        reset(db)
        db.open('test')
        db.initialize()

        # Ensure that the table is properly created within the sqlite database.
        dump = db.query(query)
        names = sorted(list(zip(*dump))[0])  # This monstrosity parses table names out of all the table values.
        self.assertEqual('heating_records', names[0])

        # Delete test database.
        db.delete_month_log()

    def test_08_sql(self):
        query = "SELECT set_temp, system_temp FROM heating_records WHERE id = 1"
        sql = f"""
              INSERT INTO heating_records (date, set_temp, system_temp)
                VALUES('{datetime.datetime.now()}', 20, 30)
              """

        db = LogComponent()
        reset(db)
        db.open('test')
        db.initialize()

        # Execute sql statement, store values as variable, delete test database, then check var values.
        db.exec(sql)
        dump = db.query(query)
        reset(db)
        self.assertEqual(dump, [(20, 30)])
