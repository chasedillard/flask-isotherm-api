import unittest
from routing import check_status
from api import app


class StatusTest(unittest.TestCase):
    def test_status_route(self):
        with app.app_context():
            output = check_status().get_json()

            self.assertEqual(output['status'], 'OK', msg='Ensure API functionality')
