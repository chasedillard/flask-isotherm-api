import sqlite3
import datetime
import os


class LogComponent:
    def __init__(self):
        self._db = None
        self.db_path = None  # Path to database.
        self._current_date = datetime.datetime.now()
        self._log_suffix = f'_{self._current_date.year}_{self._current_date.month}'  # outputs <year>_<month>
        self._file_path = os.path.dirname(os.path.realpath(__file__))
        self._root_path = os.path.join(self._file_path, os.pardir)
        self.log_path = os.path.join(self._root_path, 'logs')

    def open(self, name=None):
        """Checks if database exists, and if not, creates a new connection in the logs folder."""
        # Creates the database record with proper name and location.
        db_name = 'temp_log' if name is None else name
        self.db_path = os.path.realpath(os.path.join(self.log_path, f'{db_name}{self._log_suffix}.db'))

        # Attempts to establish database connection.
        try:
            self._db = sqlite3.connect(self.db_path)

        except sqlite3.Error as err:
            print(err.args)

    def close(self):
        """Closes database connection."""
        self._db.close()

    def initialize(self):
        """Executes SQL statements to create the required table."""
        # Assuming connection is properly made, create the heating_records table.
        try:
            # Add required table to the database records.
            c = self._db.cursor()
            sql = """
                  CREATE TABLE IF NOT EXISTS heating_records (
                    id              INTEGER PRIMARY KEY,
                    date            TEXT,
                    set_temp        INT,
                    system_temp     INT
                  )
                 """

            c.execute(sql)
            self._db.commit()

        except sqlite3.Error as err:
            print(err.args)

    def get_db(self):
        """Getter for database."""
        return self._db

    def exec(self, sql):
        """Executes SQL statements that do not have an output."""
        c = self._db.cursor()
        c.execute(sql)
        self._db.commit()

    def query(self, sql):
        """Executes SQL statements and returns output."""
        c = self._db.cursor()
        return c.execute(sql).fetchall()

    def delete_month_log(self):
        """Deletes log of the month. Used for testing purposes."""
        self.close()
        os.remove(self.db_path)
