Isotherm API
============

.. image:: https://img.shields.io/bitbucket/pipelines/ckdillard/smart-thermostat-api/master
   :alt: Passing builds
   :target: https://bitbucket.org/ckdillard/smart-thermostat-api

.. image:: https://img.shields.io/badge/version-0.1.0-informational
.. image:: https://img.shields.io/badge/License-BSD-ff69b4
   :target: https://choosealicense.com/licenses/bsd-2-clause/

Project created by Chase Dillard.

|

.. contents::

|

About
-----

This is a RESTful API written in Python using the `Flask <https://www.fullstackpython.com/flask.html>`_ framework.
It is designed with proof-of-concept webhooks for anyone to use if
your thermostat can handle webhooks and isn't smart enough to know what you want.
Dr. David T. Gray of Virginia Tech inspired this project when he talked about
it in his engineering class.

|

Using Isotherm API
------------------

In order to use the API, certain conditions must be met.

+--------------+------------------------------+
| Requirements | Sidenote                     |
+==============+==============================+
| Python       | v3.7                         |
+--------------+------------------------------+
| Pip          | Newest Version               |
+--------------+------------------------------+
| PyCharm      + Not Required But Recommended |
+--------------+------------------------------+

|

Installing Without PyCharm
^^^^^^^^^^^^^^^^^^^^^^^^^^

In order to install without PyCharm, the following commands must be run::

    cd isotherm_api
    pip install --user pipenv
    pipenv install --dev

This will install all packages required for the API to function.

|

Installing With PyCharm
^^^^^^^^^^^^^^^^^^^^^^^

With PyCharm, there is one fewer command::

    cd isotherm_api
    pip install --user pipenv

This installs pipenv for your user. From there, the pipenv interpreter can be
created within PyCharm. For more details, consult the `PyCharm documentation <https://www.jetbrains.com/help/pycharm/configuring-python-interpreter.html>`_.

|

Testing and Running the Project
-------------------------------

To test and run this project it is highly encouraged that PyCharm is used. The
configuration files contained within the ``.idea/runConfigurations``
directory can simply be executed. If PyCharm is not installed, then the following
commands can execute the project::

    # Running
    python api.py

    # Testing
    tox

