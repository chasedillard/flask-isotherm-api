from flask import render_template
from api import app
from . import routes

# If the URL is opened without any trailing API calls, it does not send a
# 404 error.
@routes.route('/')
def home():
    """
    When no route is defined, direct to main page.
    :url: <api url>/
    :return: rendered template 'home.html'
    """
    with app.app_context():
        return render_template('home.html')
