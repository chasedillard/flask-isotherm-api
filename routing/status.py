from flask import jsonify
from . import routes


@routes.route('/api/status')
def check_status():
    """
    Ensures that the API is functioning properly. If this call fails then the API is not running.
    :url: <api url>/api/status
    :return: JSON-formatted key-value pair of 'status' and 'OK'
    """
    return jsonify({'status': 'OK'})
