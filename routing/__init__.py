from flask import Blueprint

# Create variable of all of the API calls. The routes are defined in the other files within
# this directory.
routes = Blueprint('routing', __name__)

"""
Adding a function to the routes Blueprint involves using a decorator the line before the 
function call definition. 

Example:

@routes.route('/')
def function():
    return .....
"""

# Import statements to initialize package contents.
from .status import *
from .home import *
from .thermostat import *
