from flask import Flask
from templogging import *
from routing import *

# Create Flask application with name variable __name__.
# __name__ evaluates to "__main__" if this file is the first file executed.
app = Flask(__name__, template_folder='templates')

# Refer to routes variable in routing/__init__.py.
app.register_blueprint(routes)


if __name__ == '__main__':
    # Create database record to track monthly heating changes.
    db = LogComponent()
    db.open()
    db.initialize()

    # Run flask application on port 8000.
    app.run(host='localhost', port=8000, debug=True)
