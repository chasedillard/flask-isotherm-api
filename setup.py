from distutils.core import setup

setup(
    name='isotherm_api',
    version='0.1',
    packages=[],
    license='BSD',
    long_description=open('README.rst').read(),
)